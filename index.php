<?php

    require_once("animal.php");
    require_once("frog.php");
    require_once("ape.php");
    $sheep = new Animal("Shaun");
    echo "Name = " .$sheep->name ."<br>";
    echo "Legs = " .$sheep->legs ."<br>";
    echo "Cold Blooded = " .$sheep->coldblooded ."<br> <br>";

    $kodok = new Frog("Buduk");
    echo "Name = " .$kodok->name ."<br>";
    echo "Legs = " .$kodok->legs ."<br>";
    echo "Cold Blooded = " .$kodok->coldblooded ."<br>";
    echo $kodok->Lompat("Hop Hop");
    echo "<br> <br>";

    $sungokong = new Ape("Kera Sakti");
    echo "Name = " .$sungokong->name ."<br>";
    echo "Legs = " .$sungokong->legs ."<br>";
    echo "Cold Blooded = " .$sungokong->coldblooded ."<br>";
    echo $sungokong->Suara("Auooo");
?>